import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component'
import {UpdateAttoreComponent} from './update-attore/update-attore.component'
import {InserimentoComponent} from './inserimento/inserimento.component'

const routes: Routes = [
  {path:'home', component:HomeComponent},
  {path:'postAttore',component:InserimentoComponent},
  {path:'updateAttore',component:UpdateAttoreComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
