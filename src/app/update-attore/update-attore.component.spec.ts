import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAttoreComponent } from './update-attore.component';

describe('UpdateAttoreComponent', () => {
  let component: UpdateAttoreComponent;
  let fixture: ComponentFixture<UpdateAttoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAttoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAttoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
