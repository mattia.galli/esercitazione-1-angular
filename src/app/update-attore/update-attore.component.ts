import { Component, OnInit } from '@angular/core';
import { Attore } from '../entity/Attore';

@Component({
  selector: 'app-update-attore',
  templateUrl: './update-attore.component.html',
  styleUrls: ['./update-attore.component.css']
})
export class UpdateAttoreComponent implements OnInit {
//salvo attore
  attore:Attore
  constructor() { }

  ngOnInit() {
  }

  onSubmit(){
    this.attore = this.attoreService.form.value
    this.attoreService.putAttore(this.attore).subscribe(result =>{
      console.log(result)
    });
  }

  getInfoAttore(codAttore:number){
    this.attoreService.getAttoreByID(codAttore).subscribe((result:Attore)=>{
      this.attore=new Attore(result.codAttore, result.nome, result.annoNascita, result.country)
      this.attoreService.inizializeFormGroup(this.attore);
    });
  }

}
