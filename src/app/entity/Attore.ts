export class Attore{
    codAttore:number;
    nome:string;
    annoNascita:number;
    country:string;
    constructor(codAttore:number,nome:string,annoNascita:number,country:string){
        this.codAttore=codAttore;
        this.nome=nome;
        this.annoNascita=annoNascita;
        this.country=country;
    }

}