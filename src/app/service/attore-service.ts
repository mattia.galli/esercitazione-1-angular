import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import {Attore} from '../entity/Attore';
import {FormGroup, FormControl, Validators} from '@angular/forms'
@Injectable({
    providedIn:'root'
})
export class AttoreService{
    serverAddress='http://localhost:8080/api/attori'
    constructor (private http:HttpClient){ //equivale a dichiarare il modulo http 

    }
    private option={headers:new HttpHeaders().set('Access-Control-Allow-Origin','*')};

    //Oggetto di tipo FormGroup
    form: FormGroup = new FormGroup({
        codAttore: new FormControl(),
        nome: new FormControl('', Validators.required),
        annoNascita: new FormControl(1990),
        country: new FormControl('Italy')
    });
    
    getAttori(){
        return this.http.get(this.serverAddress,this.option);
    }

    addAttore(attore:Attore){
        return this.http.post(this.serverAddress, attore)
    }
    deleteAttore(codAttore:number){
        return this.http.delete(this.serverAddress+"/"+codAttore,this.option)
    }

    
}