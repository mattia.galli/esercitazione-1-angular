import { Component, OnInit } from '@angular/core';
import {Attore} from '../entity/Attore';
import {AttoreService} from '../service/attore-service';
@Component({
  selector: 'app-inserimento',
  templateUrl: './inserimento.component.html',
  styleUrls: ['./inserimento.component.css']
})
export class InserimentoComponent implements OnInit {

  constructor(public attoriService: AttoreService) { }

  ngOnInit() {
  }

  onSubmit(){
    const formAttore = this.attoriService.form.value;
    const attore = new Attore(null, formAttore.nome, formAttore.annoNascita, formAttore.country)
    console.log(attore)
    this.attoriService.addAttore(attore).subscribe(result=>{console.log(result)});
  }

}
