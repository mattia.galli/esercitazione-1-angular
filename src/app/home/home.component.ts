import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UpdateAttoreComponent} from '../update-attore/update-attore.component';
import {AttoreService} from '../service/attore-service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  //Attributi
  //Memorizzo il risultato richiesta
  result:any;
  //Memorizzo Attributi oggetto attore
  keys:any;
  router:Router;
  attoreService:AttoreService;
  constructor(router:Router, attoreService:AttoreService) { 
    this.router=router;
    this.attoreService=attoreService;
  }
 
  //metodo per richiesta al server tramite attoreService
  visualizza(){
    this.attoreService.getAttori().subscribe((attori:any[])=>{
      //assegno a variabile result le informazioni su attori
      this.result=attori;
      this.keys=Object.keys(attori[0]);
    })
  }
  performDelete(codAttore:number){
    this.attoreService.deleteAttore(codAttore).subscribe(result=>{console.log(result)});
  }

}
